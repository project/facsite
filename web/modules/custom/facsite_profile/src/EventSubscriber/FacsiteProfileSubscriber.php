<?php

namespace Drupal\facsite_profile\EventSubscriber;

use Drupal\node\Entity\Node;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Drupal\Core\Routing\TrustedRedirectResponse;

/**
 * Check profile exists or not.
 */
class FacsiteProfileSubscriber implements EventSubscriberInterface {

  /**
   * Redirect pattern based url.
   *
   * @param \Symfony\Component\HttpKernel\Event\GetResponseEvent $event
   */
  public function facsite_profileValidate(GetResponseEvent $event) {
    global $base_url;
    // get current user roles
    $roles = \Drupal::currentUser()->getRoles();
    $url = parse_url($base_url);
    $domain = explode(".", preg_replace("/^([a-zA-Z0-9].*\.)?([a-zA-Z0-9][a-zA-Z0-9-]{1,61}[a-zA-Z0-9]\.[a-zA-Z.]{2,})$/", '$2', $url['host']));
    $request = \Drupal::request();
    $request_uri = $request->server->get('REQUEST_URI', NULL);
    $current_user = \Drupal::currentUser();
    if ($request_uri != '/system/403') {
      $requestUrl = $request->server->get('HTTP_HOST', NULL);
      $requestUrl_array = explode('.', $requestUrl);
      if (!empty($requestUrl_array) && $requestUrl_array[0] != 'www' && $requestUrl_array[1] == $domain[0]) {
        $account = user_load_by_name($requestUrl_array[0]);
        if (empty($account)) {
          $event_request = $event->getRequest();
          if ($event_request->attributes->has('exception')) {
            return;
          }
          throw new NotFoundHttpException();
        }
        else {
          if (!empty($request_uri) && $request_uri != '/') {
            $arg = explode('/', $request_uri);
            if ($arg[1] == 'node' && is_numeric($arg[2]) && $arg[3] == 'edit') {
              $node = Node::load($arg[2]);
              if ($node->getOwnerId() != $current_user->id()) {
                $event_request = $event->getRequest();
                if ($event_request->attributes->has('exception')) {
                  return;
                }
                throw new NotFoundHttpException();
              }
            }
            else {
              $path = \Drupal::service('path.alias_manager')->getPathByAlias($request_uri);
              if (preg_match('/node\/(\d+)/', $path, $matches)) {
                $node = Node::load($matches[1]);
                $alias = \Drupal::service('path.alias_manager')->getAliasByPath('/node/' . $matches[1]);
                if (($node->getType() != 'page' && $node->getOwnerId() != $account->id()) || $alias == '/dashboard') {
                  $event_request = $event->getRequest();
                  if ($event_request->attributes->has('exception')) {
                    return;
                  }
                  throw new NotFoundHttpException();
                }
              }
            }
          }
          else {
            global $base_url;
            $site_url = explode('.', $base_url);
            $site_base_url = explode('//', $site_url[0]);
            if (!empty($current_user) && !empty($current_user->getUsername())) {
              $site_url[0] = $current_user->getUsername();
              $site_url[1] = $site_url[1];
            }
            if (in_array('anonymous', $roles)) {
              $hostname = implode('.', $site_url) . '/dashboard';
            }
            else {
              $hostname = $site_base_url[0] . '//' . implode('.', $site_url) . '/dashboard';
            }
            $event->setResponse(new TrustedRedirectResponse($hostname));
            // Turn caching off for this page as it is dependant on role.
            \Drupal::service('page_cache_kill_switch')->trigger();
          }
        }
      }
      else {
        $current_user = \Drupal::currentUser();
        if (in_array('faculty', $roles) && $request_uri != '/user/logout') {
          if ((!empty($requestUrl_array) && $requestUrl_array[0] == 'www' && $requestUrl_array[1] == $domain[0]) || (!empty($requestUrl_array) &&  $requestUrl_array[0] == $domain[0])) {
            global $base_url;
            $site_url = explode('.', $base_url);
            $site_base_url = explode('//', $site_url[0]);
            $site_url[0] = $current_user->getUsername();
            $site_url[2] = $site_url[1];
            $site_url[1] = $site_base_url[1];
            $hostname = $site_base_url[0] . '//' . implode('.', $site_url) . '/dashboard';
            $event->setResponse(new TrustedRedirectResponse($hostname));
            // Turn caching off for this page as it is dependant on role.
            \Drupal::service('page_cache_kill_switch')->trigger();
          }
        }
        else {
          $isFrontPage = \Drupal::service('path.matcher')->isFrontPage();
          if ($isFrontPage) {
            if (in_array('anonymous', $roles)) {
              if (!empty($request_uri) && $request_uri != '/' && $request_uri != '/dashboard' && $request_uri != '/user/login' && $request_uri != '/user/logout') {
                if ((!empty($requestUrl_array) && $requestUrl_array[0] == 'www' && $requestUrl_array[1] == $domain[0]) || (!empty($requestUrl_array) &&  $requestUrl_array[0] == $domain[0])) {
                  $event_request = $event->getRequest();
                  if ($event_request->attributes->has('exception')) {
                    return;
                  }
                  throw new NotFoundHttpException();
                }
              }
            }
            else {
              $path = rtrim($base_url, '/');
              $front_page = $path . '/user';
              $event->setResponse(new TrustedRedirectResponse($front_page));
              // Turn caching off for this page as it is dependant on role.
              \Drupal::service('page_cache_kill_switch')->trigger();
            }
          }
        }
      }
    }
  }

  /**
   * Listen to kernel.request events and call facsite_profileValidate.
   * {@inheritdoc}.
   *
   * @return array Event names to listen to (key) and methods to call (value)
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = ['facsite_profileValidate'];
    return $events;
  }

}
