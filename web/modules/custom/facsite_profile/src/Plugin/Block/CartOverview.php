<?php
/**
 * @file
 * Contains \Drupal\article\Plugin\Block\XaiBlock.
 */

namespace Drupal\airtel_vodafone\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\commerce_cart\CartProviderInterface;
use Drupal\Core\Cache\CacheableMetadata;


/**
 * Provides a 'article' block.
 *
 * @Block(
 *   id = "cart_overview_block",
 *   admin_label = @Translation("Cart Overview block"),
 *   category = @Translation("Custom Cart Overview block")
 * )
 */
class CartOverview extends BlockBase {
  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];
    $cacheable_metadata = new CacheableMetadata();
    $cacheable_metadata->addCacheContexts(['user', 'session']);
    $cartProvider = \Drupal::service('commerce_cart.cart_provider');
    $carts = $cartProvider->getCarts();
    $carts = array_filter($carts, function ($cart) {
      return $cart->hasItems();
    });
    if (!empty($carts)) {
      $cart_views = $this->getCartViews($carts);
      foreach ($carts as $cart_id => $cart) {
        $build[$cart_id] = [
          '#prefix' => '<div class="cart cart-form">',
          '#suffix' => '</div>',
          '#type' => 'view',
          '#name' => $cart_views[$cart_id],
          '#arguments' => [$cart_id],
          '#embed' => TRUE,
        ];
        $cacheable_metadata->addCacheableDependency($cart);
      }
    }
    else {
      $build['empty'] = [
        '#theme' => 'commerce_cart_empty_page',
      ];
    }
    $build['#cache'] = [
      'contexts' => $cacheable_metadata->getCacheContexts(),
      'tags' => $cacheable_metadata->getCacheTags(),
      'max-age' => $cacheable_metadata->getCacheMaxAge(),
    ];

    return $build;
  }

  /**
   * Gets the cart views for each cart.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface[] $carts
   *   The cart orders.
   *
   * @return array
   *   An array of view ids keyed by cart order ID.
   */
  protected function getCartViews(array $carts) {
    $order_type_ids = array_map(function ($cart) {
      /** @var \Drupal\commerce_order\Entity\OrderInterface $cart */
      return $cart->bundle();
    }, $carts);
    $entityTypeManager = \Drupal::entityTypeManager();
    $order_type_storage = $entityTypeManager->getStorage('commerce_order_type');
    $order_types = $order_type_storage->loadMultiple(array_unique($order_type_ids));
    $cart_views = [];
    foreach ($order_type_ids as $cart_id => $order_type_id) {
      /** @var \Drupal\commerce_order\Entity\OrderTypeInterface $order_type */
      $order_type = $order_types[$order_type_id];
      $cart_views[$cart_id] = $order_type->getThirdPartySetting('commerce_cart', 'cart_form_view', 'commerce_cart_form');
    }

    return $cart_views;
  }
}
