<?php

namespace Drupal\facsite_profile;

use Drupal;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Menu\DefaultMenuLinkTreeManipulators;
use Drupal\Core\Menu\MenuLinkInterface;
use Drupal\menu_link_content\Plugin\Menu\MenuLinkContent;

/**
 * FacsiteProfileLinkTreeManipulator service.
 *
 * @package Drupal\facsite_profile
 */
class FacsiteProfileLinkTreeManipulator extends DefaultMenuLinkTreeManipulators {

  /**
   * {@inheritdoc}
   */
  protected function menuLinkCheckAccess(MenuLinkInterface $instance) {
    global $base_url;
    $url = parse_url($base_url);
    $domain = explode(".", preg_replace("/^([a-zA-Z0-9].*\.)?([a-zA-Z0-9][a-zA-Z0-9-]{1,61}[a-zA-Z0-9]\.[a-zA-Z.]{2,})$/", '$2', $url['host']));
    $result = parent::menuLinkCheckAccess($instance);
    if ($instance instanceof MenuLinkContent) {
      $request = Drupal::request();
      $request_uri = $request->server->get('REQUEST_URI', NULL);
      if ($request_uri != '/admin/structure/menu/manage/main') {
        $requestUrl = $request->server->get('HTTP_HOST', NULL);
        $requestUrl_array = explode('.', $requestUrl);
        if (!empty($requestUrl_array) && $requestUrl_array[0] != 'www' && $requestUrl_array[1] == $domain[0]) {
          $account = user_load_by_name($requestUrl_array[0]);
          if (!empty($account)) {
            $result = $result->andIf(AccessResult::allowed());
          }
          else {
            $result = $result->andIf(AccessResult::forbidden());
          }
        }
        else {
          $result = $result->andIf(AccessResult::forbidden());
        }
      }
      \Drupal::service('page_cache_kill_switch')->trigger();
    }
    return $result;
  }

}
