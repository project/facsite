<?php

namespace Drupal\facsite_profile;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceModifierInterface;

/**
 * ServiceModifier implementation.
 *
 * @package Drupal\facsite_profile
 */
class FacsiteProfileServiceProvider implements ServiceModifierInterface {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    $container->getDefinition('menu.default_tree_manipulators')
      ->setClass(FacsiteProfileLinkTreeManipulator::class);
  }

}
