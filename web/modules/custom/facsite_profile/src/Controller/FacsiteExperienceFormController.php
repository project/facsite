<?php

namespace Drupal\facsite_profile\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Adds the ability to add education field for user..
 */
class FacsiteExperienceFormController extends ControllerBase
{
  /**
   * Returns a render-able array for the dashboard.
   */
  public function displayForm() {
    $request = \Drupal::request();
    $requestUrl = $request->server->get('HTTP_HOST', NULL);
    $requestUrl_array = explode('.', $requestUrl);
    $account = user_load_by_name($requestUrl_array[0]);

    $user = \Drupal::entityTypeManager()
      ->getStorage('user')
      ->load($account->id());
    $formObject = \Drupal::entityTypeManager()
      ->getFormObject('user', 'experience')
      ->setEntity($user);
    $form = \Drupal::formBuilder()->getForm($formObject);
    $build['form'] = $form;
    return $build;
  }

}
