<?php

namespace Drupal\facsite_profile\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Create a dashboard page.
 */
class FacsiteProfileDashboardController extends ControllerBase {

  /**
   * Returns a render-able array for the dashboard.
   */
  public function content() {
    $user_info = [];
    $user_image = '';
    $request = \Drupal::request();
    $requestUrl = $request->server->get('HTTP_HOST', NULL);
    $requestUrl_array = explode('.', $requestUrl);
    $account = user_load_by_name($requestUrl_array[0]);
    if (!empty($account)) {
      $first_name = $account->field_first_name->value;
      $last_name = $account->field_last_name->value;
      $user_image = $account->user_picture->view('large');

      $user_info[] = [
        'name' => $first_name . ' ' . $last_name,
        'email' => $account->mail->value,
        'phone' => $account->field_phone->value,
        'headline' => $account->field_headline->value,
        'profile' => $account->field_profile->value,
      ];
      return [
        // Theme hook name.
        '#theme' => 'facsite_profile',
        // Variables.
        '#user_info' => $user_info,
        '#user_image' => $user_image,
      ];
    }
    else {
      return $this->redirect('<front>');
    }
  }

}
