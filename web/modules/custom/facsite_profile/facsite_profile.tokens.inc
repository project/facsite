<?php

/**
 * @file
 * Token integration for the facsite_profile module.
 */

use Drupal\Core\Render\BubbleableMetadata;
use \Drupal\Core\Render\Markup;

/**
 * Implements hook_token_info().
 */
function facsite_profile_token_info() {
  $info['tokens']['view']['add_teaching'] = [
    'name' => t('Add teaching link'),
    'description' => t('Add teaching link'),
  ];
  $info['tokens']['view']['add_publications'] = [
    'name' => t('Add publications link'),
    'description' => t('Add publications link'),
  ];
  $info['tokens']['view']['add_awards'] = [
    'name' => t('Add awards link'),
    'description' => t('Add awards link'),
  ];
  $info['tokens']['view']['add_projects'] = [
    'name' => t('Add projects link'),
    'description' => t('Add projects link'),
  ];
  $info['tokens']['view']['add_students'] = [
    'name' => t('Add students link'),
    'description' => t('Add students link'),
  ];
  $info['tokens']['view']['profile_edit'] = [
    'name' => t('Edit user profile'),
    'description' => t('Edit profile link'),
  ];
  $info['tokens']['view']['add_education'] = [
    'name' => t('Add Education link'),
    'description' => t('Add and Edit Education on user profile.'),
  ];
  $info['tokens']['view']['add_experience'] = [
    'name' => t('Add Experience link'),
    'description' => t('Add and Edit Experience on user profile.'),
  ];
  $info['tokens']['view']['add_talks'] = [
    'name' => t('Add Talks link'),
    'description' => t('Add Talks link'),
  ];
  return $info;
}

/**
 * Implements hook_tokens().
 */
function facsite_profile_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata) {
  global $base_url;
  $url = parse_url($base_url);
  $domain = explode(".", preg_replace("/^([a-zA-Z0-9].*\.)?([a-zA-Z0-9][a-zA-Z0-9-]{1,61}[a-zA-Z0-9]\.[a-zA-Z.]{2,})$/", '$2', $url['host']));
  $url_options = ['absolute' => TRUE];
  if (isset($options['language'])) {
    $url_options['language'] = $options['language'];
  }
  $replacements = [];

  if ($type == 'view' && !empty($data['view'])) {
    /** @var \Drupal\views\ViewExecutable $view */
    $view = $data['view'];
    $teaching_link = '';
    $publications_link = '';
    $awards_link = '';
    $projects_link = '';
    $students_link = '';
    $profile_link = '';
    $request = \Drupal::request();
    $request_uri = $request->server->get('REQUEST_URI', NULL);
    if ($request_uri != '/system/403') {
      $requestUrl = $request->server->get('HTTP_HOST', NULL);
      $requestUrl_array = explode('.', $requestUrl);
      if (!empty($requestUrl_array) && $requestUrl_array[0] != 'www' && $requestUrl_array[1] == $domain[0]) {
        $account = user_load_by_name($requestUrl_array[0]);
        if (!empty($account)) {
          $current_user = \Drupal::currentUser();
          $roles = $current_user->getRoles();
          if (in_array('faculty', $roles) && $account->id() == $current_user->id()) {
            $teaching_link = '<a href="/node/add/teaching" class="button button--primary">Add Teaching</a>';
            $publications_link = '<a href="/node/add/publications" class="button button--primary">Add Publications</a>';
            $awards_link = '<a href="/node/add/awards" class="button button--primary">Add Awards</a>';
            $projects_link = '<a href="/node/add/projects" class="button button--primary">Add Projects</a>';
            $students_link = '<a href="/node/add/students" class="button button--primary">Add Students</a>';
            $profile_link = '<a href="/user/'.$current_user->id().'/edit" class="button button--primary">Edit Profile</a>';
            $education_link = '<a href="/education/edit?destination=/education">Add Education</a>';
            $experience_link = '<a href="/experience/edit?destination=/experience">Add Experience</a>';
            $talks_link = '<a href="/node/add/talks" class="button button--primary">Add Talks</a>';
          } 
        }
      }
    }
    $bubbleable_metadata->addCacheableDependency($view->storage);
    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'add_teaching':
          $replacements[$original] = Markup::create($teaching_link);
          break;
        case 'add_publications':
          $replacements[$original] = Markup::create($publications_link);
          break;
        case 'add_awards':
          $replacements[$original] = Markup::create($awards_link);
          break;
        case 'add_projects':
          $replacements[$original] = Markup::create($projects_link);
          break;
        case 'add_students':
          $replacements[$original] = Markup::create($students_link);
          break;
        case 'profile_edit':
          $replacements[$original] = Markup::create($profile_link);
          break;
        case 'add_education':
          $replacements[$original] = Markup::create($education_link);
          break;
        case 'add_experience':
          $replacements[$original] = Markup::create($experience_link);
          break;
        case 'add_talks':
          $replacements[$original] = Markup::create($talks_link);
          break;
      }
    }
  }

  return $replacements;
}

