(function ($, Drupal) {
  Drupal.behaviors.mtIsotopeMasonry = {
    attach: function (context, settings) {
      $(".views-field-field-social-links")
        .find(".field-content")
        .find("a")
        .each(function (index, abj) {
          var name = $(this).text();
          nameLowerCase = name.toLowerCase();
          var className = "fa fa-" + nameLowerCase;
          $(this).addClass(className);
          console.log(className)
        });
        //Checkbox JS for publication
        $(".publication-view .js-form-type-checkbox > .option").click(function () {
          if ($( this ).hasClass('active')) {
            $( this ).removeClass( 'active');
          } else {
            $( this ).addClass( 'active');
          }
      });
    },
  };

  $(document).ready(function() {
    // Configure/customize these variables.
    var showChar = 500;  // How many characters are shown by default
    var ellipsestext = "...";
    var moretext = "View more";
    var lesstext = "View less";
    

    $('.profile__about').find('p').each(function() {
        var content = $(this).html();
 
        if(content.length > showChar) {
 
            var c = content.substr(0, showChar);
            var h = content.substr(showChar, content.length - showChar);
 
            var html = c + '<span class="morecontent"><span>' + h + '</span><a href="" class="morelink">' + moretext + '</a></span>';
 
            $(this).html(html);
        }
 
    });
 
    $(".morelink").click(function(){
        if($(this).hasClass("less")) {
            $(this).removeClass("less");
            $(this).html(moretext);
        } else {
            $(this).addClass("less");
            $(this).html(lesstext);
        }
        $(this).parent().prev().toggle();
        $(this).prev().toggle();
        return false;
    });
        //Tabs first child 
        if (
          localStorage.getItem('navopen') !== null &&
          localStorage.getItem('navopen') !== 'About me'
        ) {
          var tab = localStorage.getItem('navopen');
          $("a[data-drupal-link-system-path='" + tab + "']").addClass('is-active');
        } else {
          $('.main-navigation ul li:first a ').addClass('is-active');
        }
        $(".main-navigation ul li  a").click(function() {
            localStorage.setItem('navopen', $(this).html());
            $('a.is-active').removeClass('is-active'); // replace former active with not-active
            $(this).addClass('is-active'); // add 
            console.log($(this).html());
        });
      

        $("li>.myid").click(function() {
          $('.myid.active').removeClass('active'); // replace former active with not-active
          $(this).addClass('active'); // add 
      });

});
//Custom select Box JS
var x, i, j, l, ll, selElmnt, a, b, c;
/*look for any elements with the class "custom-select":*/
x = document.getElementsByClassName("js-form-type-select");
l = x.length;
for (i = 0; i < l; i++) {
  selElmnt = x[i].getElementsByTagName("select")[0];
  ll = selElmnt.length;
  /*for each element, create a new DIV that will act as the selected item:*/
  a = document.createElement("DIV");
  a.setAttribute("class", "select-selected");
  a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
  x[i].appendChild(a);
  /*for each element, create a new DIV that will contain the option list:*/
  b = document.createElement("DIV");
  b.setAttribute("class", "select-items select-hide");
  for (j = 1; j < ll; j++) {
    /*for each option in the original select element,
    create a new DIV that will act as an option item:*/
    c = document.createElement("DIV");
    c.innerHTML = selElmnt.options[j].innerHTML;
    c.addEventListener("click", function(e) {
        /*when an item is clicked, update the original select box,
        and the selected item:*/
        var y, i, k, s, h, sl, yl;
        s = this.parentNode.parentNode.getElementsByTagName("select")[0];
        sl = s.length;
        h = this.parentNode.previousSibling;
        for (i = 0; i < sl; i++) {
          if (s.options[i].innerHTML == this.innerHTML) {
            s.selectedIndex = i;
            h.innerHTML = this.innerHTML;
            y = this.parentNode.getElementsByClassName("same-as-selected");
            yl = y.length;
            for (k = 0; k < yl; k++) {
              y[k].removeAttribute("class");
            }
            this.setAttribute("class", "same-as-selected");
            break;
          }
        }
        h.click();
    });
    b.appendChild(c);
  }
  x[i].appendChild(b);
  a.addEventListener("click", function(e) {
      /*when the select box is clicked, close any other select boxes,
      and open/close the current select box:*/
      e.stopPropagation();
      closeAllSelect(this);
      this.nextSibling.classList.toggle("select-hide");
      this.classList.toggle("select-arrow-active");
    });
}
function closeAllSelect(elmnt) {
  /*a function that will close all select boxes in the document,
  except the current select box:*/
  var x, y, i, xl, yl, arrNo = [];
  x = document.getElementsByClassName("select-items");
  y = document.getElementsByClassName("select-selected");
  xl = x.length;
  yl = y.length;
  for (i = 0; i < yl; i++) {
    if (elmnt == y[i]) {
      arrNo.push(i)
    } else {
      y[i].classList.remove("select-arrow-active");
    }
  }
  for (i = 0; i < xl; i++) {
    if (arrNo.indexOf(i)) {
      x[i].classList.add("select-hide");
    }
  }
}
/*if the user clicks anywhere outside the select box,
then close all select boxes:*/
document.addEventListener("click", closeAllSelect);

})(jQuery, Drupal);
